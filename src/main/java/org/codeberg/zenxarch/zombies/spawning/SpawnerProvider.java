package org.codeberg.zenxarch.zombies.spawning;

import java.util.List;

public interface SpawnerProvider {
  public List<ZombieApocalypse> zenxarch$getZombieSpawners();
}
